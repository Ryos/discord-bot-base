import debug from "debug";
import * as mongoose from "mongoose";
import {Cache, get, put} from "../objects/Cache";
import {CacheValues} from "../../lib/enum";
import {registerOptionModel} from "./models/option";

const cache = Cache.getCache();

export type DatabaseConnection = {
  db: mongoose.Connection;
}

export async function getDatabaseConnection(): Promise<DatabaseConnection> {
  const instance = get<mongoose.Connection>(CacheValues.MONGOOSE_CONNECTION, cache);

  return instance.found
    ? {db: instance.value}
    : {db: await connect()};
}

async function connect(): Promise<mongoose.Connection> {
  const log = debug("zifar");

  mongoose.connection.on("connecting", () => {
    log("database: Connecting to database"
      + ` ${process.env.ZIFAR_DB_HOST}:${process.env.ZIFAR_DB_PORT}/${process.env.ZIFAR_DB_NAME}.`);
  });

  mongoose.connection.on("connected", function() {
    log(`database: Connected to database ${process.env.ZIFAR_DB_NAME}.`);
  });

  mongoose.connection.on("disconnecting", function() {
    log(`database: Disconnecting from database ${process.env.ZIFAR_DB_NAME}.`);
  });

  mongoose.connection.on("disconnected", function() {
    log("database: Disconnected from database.");
  });

  await mongoose.connect(`mongodb://${process.env.ZIFAR_DB_HOST}:${process.env.ZIFAR_DB_PORT || 27017}/`, {
    authSource: "admin",

    auth: {
      user: <string>process.env.ZIFAR_DB_USER,
      password: <string>process.env.ZIFAR_DB_PASS
    },

    autoIndex: (process.env.NODE_ENV !== "production"),
    dbName: process.env.ZIFAR_DB_NAME,
    poolSize: (process.env.NODE_ENV === "production")
      ? parseInt(<string>process.env.ZIFAR_DB_POOL)
      : 5,
    useCreateIndex: true,
    useNewUrlParser: true
  });

  put(CacheValues.MONGOOSE_CONNECTION, mongoose.connection, cache);

  registerOptionModel(mongoose.connection);

  return mongoose.connection;
}
