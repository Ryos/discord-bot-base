import {getDatabaseConnection} from "./core/database";
import {getDiscordConnection} from "./core/bot";

getDatabaseConnection()
  .then(getDiscordConnection)
  .then();
